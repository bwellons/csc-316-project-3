# README #

This was built to show how to implement a tree structure using an adjacency list, and then to build a heap and implement Kruskal's algorithm for determining the MST (Minimum Spanning Tree). 

### What is this repository for? ###

* Builds Adjacency List
* Builds Array Based Heap
* Calculates Minimum Spanning Tree (MST) from Heap Using Kruskal's Algorithm

### How do I get set up? ###

* Put input file in root of repository
* In project3.java, change file input in public void main()
* Run

### Contribution and/or Use ###

* Not open for contribution
* Free to use for reference AFTER 5/13/17

### Who do I talk to? ###

* Brant Wellons <Brant@BrantWellons.com>