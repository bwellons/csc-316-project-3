package project3;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ArrayBasedHeapTest {

	ArrayBasedHeap heap;
	
	@Before
	public void setUp() throws Exception {
		this.heap = new ArrayBasedHeap();
	}

	@Test
	public void testArrayBasedHeap() {
		assertEquals("Array should have length of 5000", this.heap.getHeap().length, 5000);
	}

	@Test
	public void testInsert() {
		this.heap.insert(new EdgeRecord(1,2,3,null));
		assertEquals("Should set heap[0] to new edge record", this.heap.getHeap()[0].key(), 3, 0);
		assertEquals("Should be size 1 after operation", this.heap.getSize(), 1);
		
		this.heap.insert(new EdgeRecord(5,6,7, null));
		assertEquals("Should set heap[1] to new edge record", this.heap.getHeap()[1].key(), 7, 0);
		assertEquals("Should be size 2 after operation", this.heap.getSize(), 2);
		
		this.heap.insert(new EdgeRecord(8,9,10, null));
		assertEquals("Should set heap[2] to new edge record", this.heap.getHeap()[2].key(), 10, 0);
		assertEquals("Should be size 3 after operation", this.heap.getSize(), 3);
		
		this.heap.insert(new EdgeRecord(15,16,9, null));
		assertEquals("Should set heap[3] to new edge record", this.heap.getHeap()[3].key(), 9, 0);
		assertEquals("Should be size 4 after operation", this.heap.getSize(), 4);
		
		this.heap.insert(new EdgeRecord(15,16,2, null));
		assertEquals("Should set heap[3] to new edge record", this.heap.getHeap()[0].key(), 2, 0);
		assertEquals("Should set heap[1].key to 3", this.heap.getHeap()[1].key(), 3, 0);
		assertEquals("Should set heap[2].key to 10", this.heap.getHeap()[2].key(), 10, 0);
		assertEquals("Should set heap[3].key to 9", this.heap.getHeap()[3].key(), 9, 0);
		assertEquals("Should set heap[4].key to 7", this.heap.getHeap()[4].key(), 7, 0);
		assertEquals("Should be size 5 after operation", this.heap.getSize(), 5);
		
		this.heap.insert(new EdgeRecord(18,17,1, null));
		assertEquals("Should set heap[0] to new edge record", this.heap.getHeap()[0].key(), 1, 0);
		assertEquals("Should set heap[1].key to 3", this.heap.getHeap()[1].key(), 3, 0);
		assertEquals("Should set heap[2].key to 2", this.heap.getHeap()[2].key(), 2, 0);
		assertEquals("Should set heap[3].key to 9", this.heap.getHeap()[3].key(), 9, 0);
		assertEquals("Should set heap[4].key to 7", this.heap.getHeap()[4].key(), 7, 0);
		assertEquals("Should set heap[5].key to 10", this.heap.getHeap()[5].key(), 10, 0);
		assertEquals("Should be size 6 after operation", this.heap.getSize(), 6);
	}

//	@Test
//	public void testGetParentIndex() {
//		fail("Not yet implemented");
//	}
//
	@Test
	public void testIsEmpty() {
		assertEquals("List should be empty", this.heap.isEmpty(), true);
		this.heap.insert(new EdgeRecord(1,2,3,null));
		this.heap.insert(new EdgeRecord(1,2,3,null));
		assertEquals("List should NOT be empty", this.heap.isEmpty(), false);
		this.heap.deleteMin();
		assertEquals("List should NOT be empty", this.heap.isEmpty(), false);
		this.heap.deleteMin();
		assertEquals("List should be empty", this.heap.isEmpty(), true);
	}
	
	@Test
	public void testDeleteMin() {
		this.heap.insert(new EdgeRecord(1,2,3,null));
		this.heap.insert(new EdgeRecord(5,6,7, null));
		this.heap.insert(new EdgeRecord(8,9,10, null));
		this.heap.insert(new EdgeRecord(15,16,9, null));
		this.heap.insert(new EdgeRecord(15,16,2, null));
		this.heap.insert(new EdgeRecord(18,17,1, null));
		
		assertEquals("heap[0] should have key 1", this.heap.getHeap()[0].key(), 1, 0);
		this.heap.deleteMin();
		assertEquals("heap[0] should have key 2", 2, this.heap.getHeap()[0].key(), 0);
		this.heap.deleteMin();
		assertEquals("heap[0] should have key 3", 3, this.heap.getHeap()[0].key(), 0);
		this.heap.deleteMin();
		assertEquals("heap[0] should have key 7", 7, this.heap.getHeap()[0].key(), 0);
		this.heap.deleteMin();
		assertEquals("heap[0] should have key 9", 9, this.heap.getHeap()[0].key(), 0);
		assertEquals("heap[1] should have key 10", 10, this.heap.getHeap()[1].key(), 0);
	}

}
