package project3;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EdgeRecordTest {
	private EdgeRecord edgeRecord;
	
	@Before
	public void setUp() throws Exception {
		this.edgeRecord = new EdgeRecord(1, 2, 3, null);
	}

	@Test
	public void testEdgeRecord() {
		assertEquals("Should set vertex1 to 1", 1, this.edgeRecord.getVertex1());
		assertEquals("Should set vertex2 to 2", 2, this.edgeRecord.getVertex2());
		assertEquals("Should set weight to 3", 3.0, this.edgeRecord.getWeight(), 0);
		assertEquals("Should set next to null", null, this.edgeRecord.getNext());
	}

	@Test
	public void testGetWeight() {
		// Epsilon (4th arg) explained:
		// http://stackoverflow.com/questions/5686755/meaning-of-epsilon-argument-of-assertequals-for-double-values
		assertEquals("Should set weight to 3", 3.0, this.edgeRecord.getWeight(), 0);
	}

	@Test
	public void testGetVertex1() {
		assertEquals("Should set vertex1 to 1", 1, this.edgeRecord.getVertex1());
	}

	@Test
	public void testGetVertex2() {
		assertEquals("Should set vertex2 to 2", 2, this.edgeRecord.getVertex2());
	}
	
	@Test
	public void testGetNext() {
		assertEquals("Should set next to null", null, this.edgeRecord.getNext());
	}

	@Test
	public void testSetWeight() {
		this.edgeRecord.setWeight(5);
		assertEquals("Should set weight to 5", 5, this.edgeRecord.getWeight(), 0);
	}

	@Test
	public void testSetVertex1() {
		this.edgeRecord.setVertex1(6);
		assertEquals("Should set vertex1 to 6", 6, this.edgeRecord.getVertex1());
	}

	@Test
	public void testSetVertex2() {
		this.edgeRecord.setVertex2(7);
		assertEquals("Should set vertex2 to 7", 7, this.edgeRecord.getVertex2());
	}
	
	@Test
	public void testSetNext() {
		this.edgeRecord.setNext(new EdgeRecord(7, 8, 9, null));
		assertNotNull("Should not have a null next object", this.edgeRecord.getNext());
	}

}
