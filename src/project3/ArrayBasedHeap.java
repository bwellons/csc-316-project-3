package project3;

public class ArrayBasedHeap {
	EdgeRecord[] heap;
	int heapSize = 0;
	
	ArrayBasedHeap() {
		this.heap = new EdgeRecord[5000];
	}
	
	public void insert(EdgeRecord edge) {
		this.heap[this.heapSize] = edge;
		
		int childIndex = this.heapSize;
		this.heapSize++;
		
		EdgeRecord tmp = this.heap[childIndex];
		
		// Are we !root && are we less that our parent's value 
		while (childIndex > 0 && tmp.key() < this.heap[this.getParentIndex(childIndex)].key()) {
			this.heap[childIndex] = this.heap[this.getParentIndex(childIndex)];
			childIndex = this.getParentIndex(childIndex);
		}
		
		this.heap[childIndex] = tmp;
		
	}
	
	public int getParentIndex(int index) {
		return (index - 1) / 2;
	}
	
	public EdgeRecord deleteMin() {
		
		int index = 0;
		EdgeRecord deletedRecord = this.heap[index];
		
		if (this.heapSize == 0) {
			return null;
		} else if (this.heapSize == 1) {
			this.heapSize = 0;
			this.heap[0] = null;
			return deletedRecord;
		}
		this.heap[index] = this.heap[this.heapSize - 1];
		
		// Post reduce heapSize integer
		this.heap[this.heapSize--] = null;
		
		while ((index * 2) + 1 < this.heapSize || (index * 2) + 2 < this.heapSize) {
			int tmpIndex;
			if ((index * 2) + 1 < this.heapSize && (index * 2) + 2 < this.heapSize) {
				if (this.heap[(index * 2) + 1].key() < this.heap[(index * 2) + 2].key())
					tmpIndex = (index * 2) + 1;
				else
					tmpIndex = (index * 2) + 2;				
			} else  {
				tmpIndex = (index * 2) + 1;
			}
			
			if (this.heap[index].key() > this.heap[tmpIndex].key()) {
				EdgeRecord tmpRecord = this.heap[index];
				this.heap[index] = this.heap[tmpIndex];
				this.heap[tmpIndex] = tmpRecord;
				index = tmpIndex;
			} else {
				return deletedRecord;
			}
		}
		
		return deletedRecord;
	}
	
	
	
	public boolean isEmpty() {
		return this.heapSize == 0;
	}
	
	public EdgeRecord[] getHeap() {
		return this.heap;
	}
	
	// Test helper method
	public int getSize() {
		return this.heapSize;
	}
	
	public void printHeap() {
		for (int i = 0; i < this.heapSize; i++) {
			String s = "";
			EdgeRecord cur = this.heap[i];
			
			int v1 = cur.getVertex1();
			int v2 = cur.getVertex2();
			
			if (v1 > v2) {
				int tmp = v1;
				v1 = v2;
				v2 = tmp;
			}
			
			if (v1 < 10)
				s = "   " + v1;
			else if (v1 < 100)
				s = "  " + v1;
			else if (v1 < 1000)
				s = " " + v1;
			else 
				s = "" + v1;
			
			s += " ";
			
			if (v2 < 10)
				s += "   " + v2;
			else if (v2 < 100)
				s += "  " + v2;
			else if (v2 < 1000)
				s += " " + v2;
			else
				s += "" + v2;
			System.out.println(s);
		}
	}
	
}
