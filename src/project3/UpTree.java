package project3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class UpTree {
	// Priority Queue containing all edges p646
	ArrayList<EdgeRecord> upTreeNodesArray;
	ArrayList<Integer> vertices;
	item elementaryCluster[];
	
	UpTree() {
		upTreeNodesArray = new ArrayList<EdgeRecord>();
		vertices = new ArrayList<Integer>();
	}
	
	public void insertElementInOrder(int vertex1, int vertex2, float weight) {
		EdgeRecord e = new EdgeRecord(vertex1, vertex2, weight, null);
		
		if (upTreeNodesArray.size() == 0) {
			upTreeNodesArray.add(e);
		} else {
			boolean added = false;
			for (int i = 0; i < upTreeNodesArray.size() && !added; i++) {
				if (weight < upTreeNodesArray.get(i).getWeight()) {
					upTreeNodesArray.add(i, e);
					added = true;
				}
			}
			
			if (!added) {
				upTreeNodesArray.add(e);
			}
				
		}
	}
	
	public void setVertices() {
		for (int i = 0; i < upTreeNodesArray.size(); i++) {
			if (!vertices.contains(upTreeNodesArray.get(i).getVertex1())) {
				vertices.add(upTreeNodesArray.get(i).getVertex1());
			}
			if (!vertices.contains(upTreeNodesArray.get(i).getVertex2())) {
				vertices.add(upTreeNodesArray.get(i).getVertex2());
			}
		}

		Collections.sort(vertices);

	}
	
	class item {
		int parent, treeSize, index;
	}
	
	public item find(item p) {
		while (p.parent != -1) {
			p = elementaryCluster[p.parent];
		}
		
		return p;
	}
	
	public item union(item S, item T) {
		if (S.treeSize >= T.treeSize) {
			S.treeSize += T.treeSize;
			T.parent = S.index;
			return S;
		} else {
			T.treeSize += S.treeSize;
			S.parent = T.index;
			return T;
		}
	}
	
	public void MST(ArrayBasedHeap heap) {
		setVertices();
		// Will contain MST
		ArrayList<EdgeRecord> mst = new ArrayList<EdgeRecord>();
		elementaryCluster = new item[vertices.size()];

		// initialize everything to -1
		for (int i = 0; i < vertices.size(); i++) {
			elementaryCluster[i] = new item();
			elementaryCluster[i].parent = -1;
			elementaryCluster[i].treeSize = 1;
			elementaryCluster[i].index = i;
		}

		while (mst.size() < (vertices.size() - 1)) {
			EdgeRecord min = heap.deleteMin();
			int p1 = find(elementaryCluster[min.getVertex1()]).index;
			int p2 = find(elementaryCluster[min.getVertex2()]).index;
			if (p1 != p2) {
				mst.add(min);
				union(find(elementaryCluster[min.getVertex1()]), find(elementaryCluster[min.getVertex2()]));
			}
		}
		
		for (int i = 0; i < mst.size(); i++) {
			if (mst.get(i).getVertex1() > mst.get(i).getVertex2()) {
				int tmp = mst.get(i).getVertex2();
				mst.get(i).setVertex2(mst.get(i).getVertex1());
				mst.get(i).setVertex1(tmp);
			}
		}
		
		
		mst.sort(Comparator.comparing(EdgeRecord::getVertex1));
		
		for (int i = 0; i < mst.size(); i++) {
			int v1 = mst.get(i).getVertex1();
			int v2 = mst.get(i).getVertex2();
			
			if (v2 < v1) {
				int tmp = v1;
				v1 = v2;
				v2 = tmp;
			}
			
			String s = "";
			
			if (v1 < 10)
				s += "   " + v1 + " ";
			else if (v1 < 100)
				s += "  " + v1 + " ";
			else if (v1 < 1000)
				s += " " + v1 + " ";
			else 
				s += v1 + " ";
			
			if (v2 < 10)
				s += "   " + v2;
			else if (v2 < 100)
				s += "  " + v2 + " ";
			else if (v2 < 1000)
				s += " " + v2 + " ";
			else 
				s += v2 + " ";
			
			System.out.println(s);
			
		}

	}
	
	public void print() {
		for (int i = 0; i < vertices.size(); i++) {
			System.out.println(vertices.get(i));
		}
	}
}
