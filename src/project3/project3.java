package project3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class project3 {

	public static void main(String[] args) throws FileNotFoundException {
		
		ArrayBasedHeap heap = new ArrayBasedHeap();
		AdjacencyList adjList = new AdjacencyList(1000);
		UpTree upTree = new UpTree();

		String line;
	    BufferedReader br = new BufferedReader(new FileReader("./graph1-input.txt"));

		try {
			while ((line = br.readLine()) != null) {
				String[] stringArr = line.split(" ");
				int vertex1 = -1, vertex2 = -1; 
				float key = -1;
				
				for (int i = 0; i < stringArr.length; i++) {
					if (stringArr[i] != " " && stringArr[i].length() > 0) {
						if (vertex1 == -1)
							vertex1 = Integer.parseInt(stringArr[i]);
						else if (vertex2 == -1)
							vertex2 = Integer.parseInt(stringArr[i]);
						else if (key == -1) {
							key = Float.parseFloat(stringArr[i]);
						}
							
					}
				}
				if (vertex1 != -1 && vertex2 != -1 && key != -1) {
					EdgeRecord toInsert = new EdgeRecord(vertex1, vertex2, key, null);
					adjList.insert(toInsert);
					heap.insert(toInsert);
					upTree.insertElementInOrder(vertex1, vertex2, key);
				}
					
			} // end while
			
			heap.printHeap();
			upTree.MST(heap);
			adjList.printList();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
