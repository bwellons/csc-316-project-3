package project3;

import java.util.ArrayList;


/*
 * Has an entry in the array for each vertex
 * 
 * 
 */
public class AdjacencyList {
		private ArrayList<EdgeRecord> edgeRecordAdjacencyList;
	
		AdjacencyList() {
			this.edgeRecordAdjacencyList = new ArrayList<EdgeRecord>();
		}
		
		// Project guidelines say assumption of n < 1000
		// So likely use this constructor
		AdjacencyList(int vertexCount) {
			this.edgeRecordAdjacencyList = new ArrayList<EdgeRecord>();
			for (int i = 0; i < vertexCount; i++) {
				this.edgeRecordAdjacencyList.add(null);
			}
		}
		
		public void insert(EdgeRecord e) {
			handleVertex(e.getVertex1(), e.getVertex2(), e.key());
			handleVertex(e.getVertex2(), e.getVertex1(), e.key());
		}
		
		public void handleVertex(int row, int value, float key) {
			EdgeRecord cur = this.edgeRecordAdjacencyList.get(row);
			if (cur == null)
				this.edgeRecordAdjacencyList.set(row, new EdgeRecord(row, value, key, null));
			else if (value < cur.getVertex2()) {
				EdgeRecord f = new EdgeRecord(row, value, key, cur);
				this.edgeRecordAdjacencyList.set(row, f);
			} else {
				EdgeRecord next = cur.getNext();
				boolean inserted = false;
				while (next != null) {
					if (value < next.getVertex2()) {
						EdgeRecord copy = new EdgeRecord(row, value, key, next);
						cur.setNext(copy);
						next = null;
						inserted = true;
					} else {
						cur = next;
						next = next.getNext();
					}
				}
				if (inserted == false) {
					cur.setNext(new EdgeRecord(row, value, key, next));
				}
			}
		}
		
		// Retrieves head of adjacency list associated with vertex
		public EdgeRecord getEdgeRecordByVertex(int vertexValue) {
			return this.edgeRecordAdjacencyList.get(vertexValue);
		}
		
		public void printList() {
			for (int i = 0; i < this.edgeRecordAdjacencyList.size(); i++) {
				String s = "";
				if (this.edgeRecordAdjacencyList.get(i) == null) {
					
				} else if (this.edgeRecordAdjacencyList.get(i).getVertex1() == i) {
					// Use vertex 2 for output
					s = "";
					EdgeRecord cur = this.edgeRecordAdjacencyList.get(i);
					while(cur != null) {
						if (cur.getVertex2() < 10)
							s += "   " + cur.getVertex2();
						else if (cur.getVertex2() < 100)
							s += "  " + cur.getVertex2();
						else if (cur.getVertex2() < 1000)
							s += " " + cur.getVertex2();
						else 
							s += "" + cur.getVertex2();
						s += " ";
						
						cur = cur.getNext();
					}
				} else {
					// use vertex 1 for output
					s = "";
					EdgeRecord cur = this.edgeRecordAdjacencyList.get(i);
					while(cur != null) {
						if (cur.getVertex1() < 10)
							s += "   " + cur.getVertex1();
						else if (cur.getVertex1() < 100)
							s += "  " + cur.getVertex1();
						else if (cur.getVertex1() < 1000)
							s += " " + cur.getVertex1();
						else 
							s += "" + cur.getVertex1();
						s += " ";
						cur = cur.getNext();
					}
				}
				if (s.length() > 1)
					System.out.println(s.replaceAll("\\s+$", ""));
			}
		}
}
