package project3;

public class EdgeRecord {

		private int vertex1; // one end point of the edge
		private int vertex2; // another end point of the edge
		private float weight; // Real number
		private EdgeRecord next;
		int parent;
		
		EdgeRecord(int vertex1, int vertex2, float weight, EdgeRecord next) {
			this.vertex1 = vertex1;
			this.vertex2 = vertex2;
				
			this.weight = weight; // key (priority)
			this.next = next;
			this.parent = -1;
		}
		
		public float getWeight() {
			return this.weight;
		}
		
		public float key() {
			return this.weight;
		}
		
		public int getVertex1() {
			return this.vertex1;
		}
		
		public int getVertex2() {
			return this.vertex2;
		}
		
		public EdgeRecord getNext() {
			return this.next;
		}
		
		public float setWeight(float weight) {
			this.weight = weight;
			return this.weight;
		}
		
		public int setVertex1(int vertex1) {
			this.vertex1 = vertex1;
			return this.vertex1;
		}
		
		public int setVertex2(int vertex2) {
			this.vertex2 = vertex2;
			return this.vertex2;
		}
		
		public EdgeRecord setNext(EdgeRecord next) {
			this.next = next;
			return this.next;
		}
		
		public void setParent(int p) {
			this.parent = p;
		}
		
		public int getParent() {
			return this.parent;
		}
		
}
