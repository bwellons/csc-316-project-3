package project3;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AdjacencyListTest {

	private AdjacencyList adjList;
	@Before
	public void setUp() throws Exception {
		adjList = new AdjacencyList(1000);
	}


	@Test
	public void testInsert() {
		EdgeRecord first = new EdgeRecord(1,0,1, null);
		EdgeRecord second = new EdgeRecord(1,2,2,null);
		EdgeRecord third = new EdgeRecord(1,3,3,null);
		
		// Insert first, third then second to verify order gets done right
		adjList.insert(first);
		adjList.insert(third);
		adjList.insert(second);

		EdgeRecord listFirst = adjList.getEdgeRecordByVertex(0);
		assertEquals("First item should equal the first one we inserted", 0, listFirst.getVertex1());
		
		EdgeRecord listSecond = adjList.getEdgeRecordByVertex(1);
		assertEquals("row1[0] vertex2 should be 0", 0, listSecond.getVertex2());
		assertEquals("row1[1] vertex2 should be 2", 2, listSecond.getNext().getVertex2());
		assertEquals("row1[2] vertex2 should be 3", 3, listSecond.getNext().getNext().getVertex2());
		assertEquals("Should not be a fourth el", null, listSecond.getNext().getNext().getNext());
		
		adjList.insert(new EdgeRecord(1, 0, 4, null));
		assertEquals("row1[0] vertex2 should be 0", 0, listSecond.getVertex2());
		assertEquals("row1[1] vertex2 should be 0", 0, listSecond.getNext().getVertex2());
		assertEquals("row1[2] vertex2 should be 2", 2, listSecond.getNext().getNext().getVertex2());
		assertEquals("row1[2] vertex2 should be 3", 3, listSecond.getNext().getNext().getNext().getVertex2());
		
	}

}
